import random

def display_welcome_message():
    print("Welcome to the Guess the Number Game!")
    print("I have chosen a number between 1 and 100. Can you guess it?")


def generate_random_number():
    return random.randint(1, 100)


def get_user_guess():
    while True:
        guess = input("Enter your guess: ")
        if guess.isdigit():  # Check if the input is a digit
            return int(guess)
        else:
            print("Please enter a valid number.")


def display_result_message(guess, number_to_guess):
    # Vergleich muss angepasst werden.
    if guess = number_to_guess:
        print(f"Congratulations! You've guessed the number {number_to_guess}!")
    elif guess < number_to_guess:
        print("Too low! Try a higher number.")
    else:
        print("Too high! Try a lower number.")


def provide_hint(number_to_guess):
    # das x muss mit der Logik zur überprüfung ersetzt werden. Tipp: Modulo
    if number_to_guess x == 0:
        print("Hint: The number is even.")
    else:
        print("Hint: The number is odd.")


def play_game():
    display_welcome_message()

    ## Aufruf der Funktion für eine Random Zahl einfügen
    number_to_guess = 10
    attempts = 0

    while True:
        guess = get_user_guess()
        attempts += 1
        display_result_message(guess, number_to_guess)

        if guess == number_to_guess:
            print(f"It took you {attempts} attempts to guess the number.")
            break
        else:
            provide_hint(number_to_guess)


if __name__ == "__main__":
    play_game()
